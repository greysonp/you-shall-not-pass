// When the user types anything into a password box, redirect them to 
// Gandalf shouting 'You shall not pass!'
var inputs = document.getElementsByTagName('input');
for (var i = 0; i < inputs.length; i++) {
  if (inputs[i].type == 'password') {
    inputs[i].addEventListener('keypress', function() {
      window.location.href = 'https://www.youtube.com/watch?v=3xYXUeSmb-Y';
    });
  }
}
